package com.example.musiclib.repositories;

import com.example.musiclib.entities.MService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MServiceRepository extends JpaRepository<MService,Long> {
}
