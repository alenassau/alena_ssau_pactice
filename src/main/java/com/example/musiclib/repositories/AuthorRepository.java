package com.example.musiclib.repositories;

import com.example.musiclib.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author,Long> {

}



