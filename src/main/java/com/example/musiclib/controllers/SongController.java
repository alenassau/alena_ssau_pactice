package com.example.musiclib.controllers;


import com.example.musiclib.entities.Song;
import com.example.musiclib.services.AuthorService;
import com.example.musiclib.services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api/songs/")
public class SongController {
    @Autowired
    private SongService service;
    @Autowired
    private AuthorService a_service;

    //get
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Song> getSong(@PathVariable("id") Long SongId) {
        if (SongId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Song song = this.service.getById(SongId);

        if (song == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(song, HttpStatus.OK);
    }

    //post
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Song> saveSong(@RequestBody Song song) {
        if (song == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (a_service.getById(song.getAuthor().getId()) == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.service.save(song);
        return new ResponseEntity<>(song, HttpStatus.CREATED);
    }

    //put
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Song> updateSong(@RequestBody Song song, UriComponentsBuilder builder) {
        if (song == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        this.service.save(song);
        return new ResponseEntity<>(song, HttpStatus.OK);
    }


    //delete
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Song> deleteSong(@PathVariable("id") Long id) {
        Song song = this.service.getById(id);

        if (song == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.service.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Song>> getAllSongs() {
        List<Song> songs = this.service.getAll();

        if (songs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(songs, HttpStatus.OK);
    }

}
