package com.example.musiclib.controllers;

import com.example.musiclib.entities.Author;
import com.example.musiclib.entities.MService;
import com.example.musiclib.entities.Song;
import com.example.musiclib.services.MServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("/api/mservice/")
public class MServiceController {
    @Autowired
    private MServiceService service;

    //get
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MService> getMService(@PathVariable("id") Long MServiceId) {
        if (MServiceId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        MService mservice = this.service.getById(MServiceId);

        if (mservice == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mservice, HttpStatus.OK);
    }

    //post
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MService> saveMService(@RequestBody MService mservice) {
        if (mservice == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.service.save(mservice);
        return new ResponseEntity<>(mservice, HttpStatus.CREATED);
    }

    //put
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MService> updateMService(@RequestBody MService mservice, UriComponentsBuilder builder) {
        if (mservice == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.service.save(mservice);
        return new ResponseEntity<>(mservice, HttpStatus.OK);
    }

    public static class MServiceSong implements Serializable {
        public MService mservice;
        public Song song;

        public MServiceSong() {
            this.song = null;
            this.mservice = null;
        }

        public MService getMService() {
            return mservice;
        }

        public void setService(MService ms) {
            this.mservice = ms;
        }

        public Song getSong() {
            return song;
        }

        public void setSong(Song song) {
            this.song = song;
        }


    }

    //put
    @RequestMapping(value = "add/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MService> addSongToMService(@RequestBody MServiceSong mservice_song, UriComponentsBuilder builder) {
        MService mservice = mservice_song.getMService();
        Song song = mservice_song.getSong();
        if (mservice == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (song == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        service.getById(mservice.getId()).getSongs().add(song);

        this.service.save(mservice);
        return new ResponseEntity<>(mservice, HttpStatus.OK);
    }

    //delete
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MService> deleteMService(@PathVariable("id") Long id) {
        MService mservice = this.service.getById(id);

        if (mservice == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.service.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<MService>> getAllMServices() {
        List<MService> mservices = this.service.getAll();

        if (mservices.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mservices, HttpStatus.OK);
    }
}
