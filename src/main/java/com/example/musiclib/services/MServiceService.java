package com.example.musiclib.services;


import com.example.musiclib.entities.MService;
import com.example.musiclib.repositories.AuthorRepository;
import com.example.musiclib.repositories.MServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Component
@Service
public class MServiceService {
    @Autowired
    private MServiceRepository repository;

    @PersistenceContext
    EntityManager em;

    public MService getById(Long id) {
        return repository.findById(id).orElse(null);
    }


    @Transactional
    public MService save(MService author) {
        return em.merge(author);
    }


    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<MService> getAll() {
        return repository.findAll();
    }
}
