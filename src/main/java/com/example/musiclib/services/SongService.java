package com.example.musiclib.services;


import com.example.musiclib.entities.Song;
import com.example.musiclib.repositories.SongRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Component
@Service
public class SongService {
    @Autowired
    private SongRepository repository;

    @PersistenceContext
    EntityManager em;

    public Song getById(Long id) {
        return repository.findById(id).orElse(null);
    }


    @Transactional
    public Song save(Song song) {
        return em.merge(song);
    }


    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Song> getAll() {
        return repository.findAll();
    }

}
